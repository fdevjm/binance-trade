import React, { Component } from 'react';

import History from './components/single-history'


const dummyData = {
  coin: 'STORJ',
  amount: 1466.66484230,
  avgPrice: 0.00017000,
  buyPrice: 0.55342321,
  profit: 0.45
}

class App extends Component {
  render() {
    return (
      <div className="contents">
        <History
          coin={dummyData.coin}
          amount={dummyData.amount}
          avgPrice={dummyData.avgPrice}
          buyPrice={dummyData.buyPrice}
          profit={dummyData.profit}/>
      </div>
    );
  }
}

export default App;
