import React from 'react'

export default ({
  coin, amount, avgPrice, buyPrice, profit
}) => (
  <div className="row">
    <div className="field">
      { coin }
    </div>
    <div className="field">
      <span>{ amount }</span>
      <span className="small-grey">{ coin }</span>
    </div>
    <div className="field">
      <span>{ avgPrice }</span>
      <span className="small-grey">BTC</span>
    </div>
    <div className="field">
      <span>{ buyPrice }</span>
      <span className="small-grey">BTC</span>
    </div>
    <div className="field">
      <span>{ profit }%</span>
      <div>0.00332244<span className="small-grey">BTC</span></div>
    </div>
  </div>
)